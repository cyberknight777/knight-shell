#[allow(unused_imports)]
use std::io::{stdin, stdout, Write};
use std::process::{Command, Stdio, Child};
use std::env;
use std::path::Path;
fn main() {
    #[allow(unused_must_use)]
    #[allow(unused_variables)]
    loop {
	print!("\n\x1b[1;36mknight-shell \x1b[1;32m~>\x1b[0m \x1b[1;37m");
	stdout().flush();
	let mut input = String::new();
	stdin().read_line(&mut input).unwrap();
	let mut cmds = input.trim().split(" | ").peekable();
	let mut previous_cmd = None;
	while let Some(cmd) = cmds.next() {
	    let mut parts = input.trim().split_whitespace();
	    let cmd = parts.next().unwrap();
	    let args = parts;
	    match cmd {
		"cd" => {
		    let new_dir = args.peekable().peek().map_or("/", |x| *x);
		    let root = Path::new(new_dir);
		    if let Err(e) = env::set_current_dir(&root) {
			eprintln!("{}", e);
		    }
		    previous_cmd = None;
		},
		"exit" => return,
		":" => print!(""),
		cmd => {
		    let stdin = previous_cmd.map_or(Stdio::inherit(),|output: Child| Stdio::from(output.stdout.unwrap()));
		    let stdout = if cmds.peek().is_some() {
			Stdio::piped()
		    } else {
			Stdio::inherit()
		    };
		    let output = Command::new(cmd).args(args).stdin(stdin).stdout(stdout).spawn();
		    match output {
			Ok(output) => { previous_cmd = Some(output); },
			Err(e) => {
			    previous_cmd = None;
			    eprintln!("{}", e);
			},
		    };
		}
	    }
	}
	if let Some(mut final_cmd) = previous_cmd {
	    final_cmd.wait();
	}
    }
}
